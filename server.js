
// var path = require('path');
var bodyParser = require('body-parser');
// => db.js
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test', {useNewUrlParser: true});

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  // we're connected!
  console.log("We're connected!");
});
// => db.js

var claims = new mongoose.Schema({
    // _id: mongoose.Schema.Types.ObjectId,
    name: String,
    surname: String,
    ssn: String,
    desc: String
});

var Claim = mongoose.model('Claim', claims);

const express = require('express');
// var router = express.Router();

// const cl = new Claim({ name: 'Adam', surname: 'Lallana'});
// cl.save().then(() => console.log('Done'));

var app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

var cors = require('cors');
app.use(cors({ origin: 'http://localhost:4200', methods: 'GET, POST, OPTIONS, PUT, PATCH, DELETE' }));


// app.get('/', (req, res) => res.send('Hello World!'));

// => localhost:8080/Claims
app.get('/Claims', (req, res) => {
    Claim.find({}, function(err, docs) {
        if(err) {
            res.send(err);
        } else {
            res.send(docs);
        }
    })
})
var ObjectId = require('mongoose').Types.ObjectId;

app.get('/Claims/Id/:id', (req, res) => {
    // var i = ObjectId.createFromHexString(req.params.id);
    // console.log("Id: " + i);

    // if (!ObjectId.isValid(req.params.id))
    //     return res.status(400).send(`No record with given id : ${req.params.id}`);

    // Claim.findById(req.params.id, (err, doc) => {
    //     if (!err) { res.send(doc); }
    //     else { console.log('Error in Retriving Employee :' + JSON.stringify(err, undefined, 2)); }
    // });

    // Claim.findById(req.params.id, function (err, claim) {});

    // var id = new ObjectId(req.params.id);

    // if (id.match(/^[0-9a-fA-F]{24}$/)) {
    //     // Yes, it's a valid ObjectId, proceed with `findById` call.
    //     console.log("You da best!");
    //   }

    if(ObjectId.isValid(req.params.id)){ 
        Claim.findById(req.params.id, function(err, doc) {
            if(err) {
                res.send(err);
            } else {
                res.send(doc);
            }
        })
    } else {
        // redirect to error page
    }
});

// /Claim/Name?name=Steven
app.get('/Claims/Name', (req, res) => {
    // TODO: investigate why ObjectId.isValid() fails
        Claim.find({ name: req.query.name }, function(err, doc) {
            if(err) {
                res.send(err);
            } else {
                res.send(doc);
            }
        })  
});

app.listen(8080, () => console.log('Server started at port : 8080'));
