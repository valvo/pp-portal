import { AccessGuard } from './AccessGuard';
import { SearchResultsComponent } from './search-results/search-results.component';
import { ClaimDetailComponent } from './claim-detail/claim-detail.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeViewComponent } from './home-view/home-view.component';
import { FormsModule } from '@angular/forms';


const routes: Routes = [
  { path: 'home', component: HomeViewComponent },
  // { path: 'claim/:id', component: ClaimDetailComponent},
  { path: 'claim/:_id', component: ClaimDetailComponent, data:{requiresLogin: true}, canActivate: [ AccessGuard ]},
  { path: '', redirectTo: 'home', pathMatch: 'full'},
  { path: 'search-results', component: SearchResultsComponent}
  // { path: '**', component: PageNotFoundComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes), FormsModule],
  exports: [RouterModule]
})
export class AppRoutingModule { }
