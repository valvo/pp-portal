import { SharedService } from './../shared.service';
import { Claim } from './../claim';
import { Component, OnInit, Input, Output } from '@angular/core';
import {
  trigger,
  state,
  style,
  animate,
  transition,
  AnimationEvent
  // ...
} from '@angular/animations';
import { CommonService } from '../common.service';
import { FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
  animations: [
    trigger('openClose', [
      // ...
      state('open', style({
        height: '200px',
        opacity: 1,
        backgroundColor: '#fafafa'
      })),
      state('closed', style({
        height: '0px',
        // opacity: 0.5,
        opacity: 0,
        backgroundColor: '#fafafa'
      })),
      transition('open => closed', [
        animate('0.5s')
      ]),
      transition('closed => open', [
        animate('0.5s')
      ]),
    ]),
  ],
})
export class SearchComponent implements OnInit {

  searchOptionControl = new FormControl('', Validators.required);

  searchById: string = '';
  searchByName: string = '';
  searchBySurname: string = '';
  searchBySSN: string = '';

  searchOptionList: string[] = ['Search by Id', 'Search by name', 'Search by surname', 'Search by ssn'];

  searchOption: string = 'Search by Id';

  claims:  Claim[] = [];

  emptyData: boolean = this.claims.length === 0;

  constructor(private service: CommonService, private sharedService: SharedService) { }

  ngOnInit(): void {
    this.sharedService.sharedListOfClaims.subscribe(claims => this.claims = claims);
  }

  isOpen = true;

  isSearchDropdownOpen = false;

  toggle() {
    this.isOpen = !this.isOpen;
  }

  toggleDropdown() {
    this.isSearchDropdownOpen = !this.isSearchDropdownOpen;
  }

  @Input() logging = false;
  onAnimationEvent ( event: AnimationEvent ) {
    if (!this.logging) {
      return;
    }
    // openClose is trigger name in this example
    console.warn(`Animation Trigger: ${event.triggerName}`);

    // phaseName is start or done
    console.warn(`Phase: ${event.phaseName}`);

    // in our example, totalTime is 1000 or 1 second
    console.warn(`Total time: ${event.totalTime}`);

    // in our example, fromState is either open or closed
    console.warn(`From: ${event.fromState}`);

    // in our example, toState either open or closed
    console.warn(`To: ${event.toState}`);

    // the HTML element itself, the button in this case
    console.warn(`Element: ${event.element}`);
  }

  clear(): void {
    this.claims = [];
    this.emptyData = true;
  }

  findById(value: string) {
    this.clear();
    this.service.getById(value).subscribe((data) => {
      this.claims.push(data as Claim);
      this.sharedService.updateClaimList(this.claims);
    });
  }

  findByName(name: string) {
    this.clear();
    this.service.getByName(name).subscribe((data: Claim[]) => {
      this.claims = data; 
      this.sharedService.updateClaimList(this.claims);
    });
  }

  onSearchOptionSelect(event: any) {
    this.searchOption = event.target.value;
  }

  // FIXME 
  // find more elegant way of handling this
  find() {
    if(this.searchOption === 'Search by Id') {
      this.findById(this.searchById);
    }
    if(this.searchOption === 'Search by name') {
      this.findByName(this.searchByName);
    }
  }

}
