import { SharedService } from './../shared.service';
import { User } from './../user';
import { Component, OnInit, Input } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';


@Component({
  selector: 'app-authentication-modal',
  templateUrl: './authentication-modal.component.html',
  styleUrls: ['./authentication-modal.component.css']
})
export class AuthenticationModalComponent implements OnInit {

  constructor(private sharedService: SharedService) { }
  userObj: User = {name: '', password: '', role: ''};
  user = { username: '', password: ''};
  form: FormGroup;

  @Input() usr: User;

  ngOnInit(): void {
    this.form = new FormGroup({
      'username': new FormControl(this.user.username, [
        Validators.required,
        Validators.minLength(4)
      ]),
      'password': new FormControl(this.user.password, [
        Validators.required,
        Validators.minLength(4)
      ]),
    }); // <-- add custom validator at the FormGroup level
  }

  get name() { return this.form.get('username'); }
  get password () { return this.form.get('password'); }

  showAuthenticationModal : boolean = false;

  onToggleAuthenticationModal() {
    this.showAuthenticationModal = !this.showAuthenticationModal;
    this.form.reset();
  }

  authenticate() {
    let usr =  this.form.value['username'];
    let pwd = this.form.value['password'];
    this.userObj.name = usr;
    this.userObj.password = pwd;
    this.sharedService.storeUser(this.userObj);
    this.onToggleAuthenticationModal();
  }

}
