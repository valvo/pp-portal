import { SharedService } from './../shared.service';
import { Claim } from './../claim';
import { CommonService } from './../common.service';
import { Component, OnInit, Input, Output } from '@angular/core';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {

  // msg: string = "I'm a search results component!";

  // @Input() claim: Claim;

  // message: string;

  isWait: boolean;

  public claims: Claim[];

  isClaimListEmpty: boolean = true;

  constructor(private data: CommonService, private sharedService: SharedService) { 
    this.isWait = true;
    setTimeout(() => {
     this.isWait = false;
    }, 2000);
  }

  ngOnInit(): void {
    // this.data.message.subscribe(message => this.message = message);
    this.sharedService.sharedListOfClaims.subscribe(data => this.claims = data);
  }

}
