
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/Rx';
import { Claim } from './claim';

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  claims: Claim[];

  readonly baseURL = 'http://localhost:8080/Claims';

  constructor(private http: HttpClient) { }

  // saveUser(user) {
  //   return this.http.post('http://localhost:8080/api/SaveUser/', user)
  //   .pipe(map((response: Response) => response.json()))
  // }

  // getUser() {
  //   return this.http.get('http://localhost:8080/api/getUser/')
  //   .pipe(map((response: Response) => response.json()))
  // }

  // deleteUser(id) {
  //   return this.http.post('http://localhost:8080/api/deleteUser/', {'id': id})
  //   .pipe(map((response: Response) => response.json()))
  // }

  getData() {
    return this.http.get(this.baseURL);
  }

  getById(id: string) {
    return this.http.get(this.baseURL + '/Id/' + id);
  }

  getByName(name: string) {
    return this.http.get(this.baseURL + '/Name?name=' + name);
  }

}
