import { User } from './user';
import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { Claim } from './claim';

@Injectable({
  providedIn: 'root'
})
export class SharedService {

  private claimList = new BehaviorSubject<Claim[]>([]);
  sharedListOfClaims = this.claimList.asObservable();
  private user = new BehaviorSubject<User>(null);
  sharedUser = this.user.asObservable();

  constructor() { }

  updateClaimList(arr: Claim[]): void {
    this.claimList.next(Object.assign([], arr)); // <- resets claimList
    // this.claimList.next([...this.claimList.getValue(), ...arr]);
  }

  storeUser(user: User) {
    this.user.next(user);
  }

}
