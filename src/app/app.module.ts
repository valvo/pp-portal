import { AccessGuard } from './AccessGuard';
import { MaterialModule } from './material/material.module';
import { SharedService } from './shared.service';
import { CommonService } from './common.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeViewComponent } from './home-view/home-view.component';
import { ClaimDetailComponent } from './claim-detail/claim-detail.component';
import { AuthenticationModalComponent } from './authentication-modal/authentication-modal.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SearchComponent } from './search/search.component';
import { HttpClientModule } from '@angular/common/http';
import { SearchResultsComponent } from './search-results/search-results.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeViewComponent,
    ClaimDetailComponent,
    AuthenticationModalComponent,
    SearchComponent,
    SearchResultsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MaterialModule
  ],
  providers: [CommonService, SearchComponent, SharedService, AccessGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
