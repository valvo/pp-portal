import { Claim } from './claim';

export const CLAIMS: Claim[] = [
  {
    _id: 11,
    name: 'Steven',
    surname: 'Spielberg',
    ssn: 111,
    custNo: 1111,
    address: 'Manchester, UK, United street 79-41',
    postCode: 8080,
    phoneNum: '+44-715-550-4370',
    desc: 'shit',
  },
  {
    _id: 22,
    name: 'Michael',
    surname: 'Bay',
    ssn: 222,
    custNo: 2222,
    address: 'Liverpool, UK, Gatsby street 112-73',
    postCode: 2000,
    phoneNum: '+44-725-553-8026',
    desc:
      'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam semper diam at erat pulvinar, at pulvinar felis blandit. Vestibulum volutpat tellus diam, consequat gravida libero rhoncus ut. Morbi maximus, leo sit amet vehicula eleifend, nunc dui porta orci, quis semper odio felis ut quam.',
  },
  {
    _id: 33,
    name: 'Martin',
    surname: 'Wilson',
    ssn: 333,
    custNo: 3333,
    address: 'London, UK, Oxford street 23-45',
    postCode: 3001,
    phoneNum: '+44-775-555-1540',
    desc: 'shit shit shit',
  },
  {
    _id: 44,
    name: 'Harry',
    surname: 'Kane',
    ssn: 444,
    custNo: 4444,
    address: 'Sweden, Stockholm, Molmo street 11-30',
    postCode: 11001,
    phoneNum: '+44-765-551-5415',
    desc:
      'Suspendisse varius ligula in molestie lacinia. Maecenas varius eget ligula a sagittis. Pellentesque interdum, nisl nec interdum maximus, augue diam porttitor lorem, et sollicitudin felis neque sit amet erat. Maecenas imperdiet felis nisi, fringilla luctus felis hendrerit sit amet. Aenean vitae gravida diam, finibus dignissim turpis. Sed eget varius ligula, at volutpat tortor.',
  },
];
