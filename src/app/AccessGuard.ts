import { USERS } from './mock-users';
import { SharedService } from './shared.service';
import { User } from './user';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable()
export class AccessGuard implements CanActivate {

    constructor(private sharedService: SharedService) { }

    public user: User;

    canActivate(
        route: ActivatedRouteSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        const requiresLogin = route.data.requiresLogin || false;
        if (requiresLogin) {
            // Check that the user is logged in...
            this.sharedService.sharedUser.subscribe(data => this.user = data);
            for(let user of USERS) {
                if(user.name === this.user.name && user.password === this.user.password) {
                    return true;
                }
            }
            return false;
        }
    }
}
