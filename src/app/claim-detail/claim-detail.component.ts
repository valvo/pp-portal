import { SharedService } from './../shared.service';
import { SearchComponent } from './../search/search.component';
import { CommonService } from './../common.service';
import { ClaimService } from './../claim.service';

// import { CLAIMS } from './../mock-claims';
import { Claim } from './../claim';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { Observable, of } from 'rxjs';

@Component({
  selector: 'app-claim-detail',
  templateUrl: './claim-detail.component.html',
  styleUrls: ['./claim-detail.component.css']
})
export class ClaimDetailComponent implements OnInit {
  claim: Claim;
  // claims = CLAIMS;
  username: string;

  constructor(
    private route: ActivatedRoute,
    private claimService: ClaimService,
    private location: Location,
    private commonService: CommonService,
    private searchComponent: SearchComponent,
    private sharedService: SharedService
  ) {}

  ngOnInit(): void {
    // this.getClaim();
      this.route.params.subscribe(params => {
      let param = params['_id'];
      if(param) {
        return of(this.sharedService.sharedListOfClaims.subscribe(claims => {
          claims.find(cl => cl._id == param ? this.claim = cl : '');
        }))
      }
    })
  }

  // DO NOT REMOVE

  // getClaim(): void {
  //   const id = +this.route.snapshot.paramMap.get('id');
  //   this.claimService.getClaim(id)
  //   .subscribe(claim => this.claim = claim);
  // }

  goBack(): void {
    this.location.back();
  }

}
