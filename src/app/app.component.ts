import { SharedService } from './shared.service';
import { CommonService } from './common.service';
import { Component } from '@angular/core';
import { User } from './user';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [
    // animation triggers go here
  ]
})
export class AppComponent {
  title = 'pp-portal';

  constructor(private service: CommonService, private sharedService: SharedService) {

  }

  mongoData;

  user: User;

  ngOnInit() {
    this.service.getData().subscribe(data => this.mongoData = data);

    this.sharedService.sharedUser.subscribe(data => this.user = data);
  }

  // onSave = function (user, isValid: boolean) {
  //   user.mode = this.valbutton;
  //   this.newService.saveUser(user)
  //   .subscribe(data => { alert(data.data);
  //     this.ngOnInit();
  //   }
  //   , error => this.errorMessage = error)
  // }

  // edit = function(kk) {
  //   this.id = kk._id;
  //   this.name = kk.name;
  //   this.address = kk.address;
  //   this.valbutton = "Update";
  // }

  // delete = function(id) {
  //   this.newService.deleteUser(id)
  //   .subscribe(data => { alert(data.data) ; this.ngOnInit();}, error => this.errorMessage = error )
  // }

}
