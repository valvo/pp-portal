export interface User {
    name: string;
    role: string;
    password: string;
}