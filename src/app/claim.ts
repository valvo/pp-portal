export interface Claim {
    _id: number;
    name: string;
    surname: string;
    ssn: number;
    custNo: number;
    address: string;
    postCode: number;
    phoneNum: string;
    desc: string;
  }