import { CLAIMS } from './mock-claims';
import { Claim } from './claim';
import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class ClaimService {

  constructor() { }

  getClaims(): Observable<Claim[]> {
    // TODO: send the message _after_ fetching the heroes
    // this.messageService.add('HeroService: fetched heroes');
    return of(CLAIMS);
  }

  getClaim(id: number): Observable<Claim> {
    // TODO: send the message _after_ fetching the hero
    // this.messageService.add(`ClaimService: fetched claim id=${id}`);
    return of(CLAIMS.find(claim => claim._id === id));
  }

  
}
